//
//  WODHealthKitAPI.h
//  WOD
//
//  Created by Niko on 8/5/14.
//  Copyright (c) 2014 DailyBurn, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WorkoutVideo;

@interface WODHealthKitAPI : NSObject <UIAlertViewDelegate>

/*
 returns YES if the device supports healthkit, AND we have not asked already.
 */
+(BOOL)shouldRequestHealthKitAccess;

/*
 begins a request flow that does the following:
 
 -presents user with explanatory alertview
 
 -if the user says they'd like us to request HealthKit access,
 we perform the HealthKit permission request, and the completion 
 block is executed when the healthkit modal is dismissed.
 
 -if the user does not want us to request access, we execute
 the completion block and do nothing further
 
 */
+(void)beginHealthKitRequestFlowWithCompletion:(void(^)(BOOL allowedAccess))completion;

/*
 Units for this is Beats Per Minute
 */
+ (void)saveHeartRateBPM:(NSNumber *)heartRate;

/*
 Units for this is Kilocalories
 */
+(void)completedWorkout:(WorkoutVideo*)workout CalsBurned:(NSNumber*)calsBurned;

@end
