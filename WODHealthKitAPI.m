//
//  WODHealthKitAPI.m
//  WOD
//
//  Created by Niko on 8/5/14.
//  Copyright (c) 2014 DailyBurn, Inc. All rights reserved.
//

#import "WODHealthKitAPI.h"
#import "WODAnalyticsAPI.h"
#import "WorkoutVideo+Additions.h"
#import "NSMutableDictionary+NilCheck.h"
#import "Trainer+Additions.h"

@import HealthKit;

@implementation WODHealthKitAPI

static HKHealthStore *healthStore;

static NSString * const kRequestedHealthKitAccessKey = @"kRequestedHealthKitAccessKey";
static NSString * const kHealthKitAuthStatusKey = @"kHealthKitAuthStatusKey";

static BOOL requestedHealthKitAccess = NO;
static BOOL allowedHealthKitAccess = NO;

#pragma mark - Private Methods

+(void)initialize{
    
    if (self == [WODHealthKitAPI class]) {
        
        allowedHealthKitAccess = [[NSUserDefaults standardUserDefaults] boolForKey:kHealthKitAuthStatusKey];
        requestedHealthKitAccess = [[NSUserDefaults standardUserDefaults] boolForKey:kRequestedHealthKitAccessKey];
        
        if ([HKHealthStore isHealthDataAvailable]) {
            healthStore = [[HKHealthStore alloc] init];
        }
    }
}

+(BOOL)canWriteToHealthKit{
    
    return (allowedHealthKitAccess && healthStore != nil);
    
}

+(void)setAllowedHealthKitAccess:(BOOL)allowed{
    
    allowedHealthKitAccess = allowed;
    
    [[NSUserDefaults standardUserDefaults] setBool:allowedHealthKitAccess
                                            forKey:kHealthKitAuthStatusKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)setRequestedHealthKitAccess:(BOOL)requested{
    
    requestedHealthKitAccess = requested;
    
    [[NSUserDefaults standardUserDefaults] setBool:requestedHealthKitAccess
                                            forKey:kRequestedHealthKitAccessKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Public methods

+(BOOL)shouldRequestHealthKitAccess{
    
    if (requestedHealthKitAccess == NO && healthStore != nil) {
        return YES;
    } else {
        return NO;
    }
}

static NSInteger const kAlertViewHKAccessTag = 900;

static void (^accessCompletionBlock)(BOOL allowedAccess) = nil;

+(void)beginHealthKitRequestFlowWithCompletion:(void(^)(BOOL allowedAccess))completion{
    
    [self setRequestedHealthKitAccess:YES];
    
    accessCompletionBlock = completion;

    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Allow Access?"
                                                 message:@"Beautifully articluated message about why we're requesting HealthKit access"
                                                delegate:self
                                       cancelButtonTitle:@"Yes, I understand" //yolo
                                       otherButtonTitles:@"No", nil];
    av.tag = kAlertViewHKAccessTag;
    
    [av performSelectorOnMainThread:@selector(show)
                         withObject:nil
                      waitUntilDone:NO];
}

+(void)requestHealthKitAccess{
    
    NSSet *writeDataTypes = [self dataTypesToWrite];
    NSSet *readDataTypes = [self dataTypesToRead];
    
    /*
     completion block for HealthKit access request
     */
    void (^completeBlock)(BOOL success, NSError *error) = ^(BOOL success, NSError *error){
        
        //TODO: winback message if we get denied access
        
        if (!success) {
            NSLog(@"You didn't allow HealthKit to access these read/write data types. %@", error.localizedDescription);
        }
        
        /*
         
         record the result so we know what to expect next time
         this allows us to drop analytics events for auth requests that are already allowed.
         
         */
        [self setAllowedHealthKitAccess:success];
        
        /*
         The completion block passed in by the caller who began the request flow
         */
        if (accessCompletionBlock != nil) {
            accessCompletionBlock(success);
        }
        accessCompletionBlock = nil; //cleanup
        
        { //Analytics
            
            if (allowedHealthKitAccess != success) { //we only care if our auth status is changing
                
                NSString * const accessRequestMsg = @"HealthKit Access Request";
                
                NSDictionary *dict;
                
                if (success) {
                    
                    dict = @{ accessRequestMsg : @"Allowed" };
                    
                } else {
                    
                    NSMutableDictionary *mut = [@{ accessRequestMsg : @"Denied" } mutableCopy];
                    
                    [mut safeSetObject:error.localizedDescription forKey:@"Error"];
                    
                    dict = mut;
                    
                }
                
                [WODAnalyticsAPI trackEvent:accessRequestMsg
                                 WithParams:dict];
            }
            
        }
    };
    
    [healthStore requestAuthorizationToShareTypes:writeDataTypes
                                        readTypes:readDataTypes
                                       completion:completeBlock];
}

#pragma mark - UIAlertViewDelegate

+ (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == kAlertViewHKAccessTag){
        
        NSLog(@"tapped button at index %li", (long)buttonIndex);
        
        if (buttonIndex == alertView.cancelButtonIndex) { //we are intentionally using the cancelButton as OK button, to get the styling
            [self requestHealthKitAccess];
        } else {
            accessCompletionBlock(NO);
            accessCompletionBlock = nil;
        }
    }
}

#pragma mark - Writing to HealthKit

//let heartRateUnit = HKUnit(fromString: "count/min") // or HKUnit.countUnit().unitDividedByUnit(HKUnit.minuteUnit())
static NSString * const kUnitsString = @"count/min";

+ (void)saveHeartRateBPM:(NSNumber *)heartRate {
    
    if ([self canWriteToHealthKit]) {
        
        HKQuantityType *quantityType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
        
        HKUnit *heartRateUnit = [HKUnit unitFromString:kUnitsString];
        
        HKQuantity *quantity = [HKQuantity quantityWithUnit:heartRateUnit
                                                doubleValue:heartRate.doubleValue];
        
        NSDate *now = [NSDate date];
        
        HKQuantitySample *sample = [HKQuantitySample quantitySampleWithType:quantityType
                                                                   quantity:quantity
                                                                  startDate:now
                                                                    endDate:now];
        
        [healthStore saveObject:sample
                 withCompletion:^(BOOL success, NSError *error){
             
                 if (success) {
                     NSLog(@"saved heartrate data");
                 } else {
                     NSLog(@"error saving heartrate data");
                 }
                 
                 if (error) {
                     NSLog(@"%@", error.localizedDescription);
                 }
                 
             }];
    }
}

+(void)completedWorkout:(WorkoutVideo*)workout CalsBurned:(NSNumber*)calsBurned{
    
    if ([self canWriteToHealthKit]) {
        
        HKUnit *calsUnit = [HKUnit unitFromEnergyFormatterUnit:NSEnergyFormatterUnitKilocalorie];
        
        HKQuantity *calsQuantity = [HKQuantity quantityWithUnit:calsUnit
                                                    doubleValue:calsBurned.doubleValue];
        
        NSDate *now = [NSDate date];
        
        NSDictionary *metaData = [self metaDataForWorkout:workout];
        
        HKWorkout *workout = [HKWorkout workoutWithActivityType:HKWorkoutActivityTypeMixedMetabolicCardioTraining
                                                      startDate:now
                                                        endDate:now
                                                  workoutEvents:nil
                                              totalEnergyBurned:calsQuantity
                                                  totalDistance:nil
                                                       metadata:metaData];
        [healthStore saveObject:workout
                 withCompletion:^(BOOL success, NSError *error){

                     if (success) {
                         NSLog(@"saved workout data");
                     } else {
                         NSLog(@"error saving workout data");
                     }
                     
                     NSLog(@"%@", metaData.description);
                     
                     if (error) {
                         NSLog(@"%@", error.localizedDescription);
                     }
                     
                 }];
    }
}

static NSString *const kTitleKey = @"workoutTitle";
static NSString *const kDescrptionKey = @"workoutDescription";
static NSString *const kTrainerKey = @"trainer";
static NSString *const kImageURLKey = @"boxArt";
static NSString *const kDurationKey = @"workoutLength";

+(NSDictionary*)metaDataForWorkout:(WorkoutVideo*)workout{
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    [dict safeSetObject:workout.workoutVideoTitle
                 forKey:kTitleKey];
    [dict safeSetObject:workout.workoutVideoDescription
                 forKey:kDescrptionKey];
    
    Trainer *t = workout.trainers.anyObject;
    
    [dict safeSetObject:t.fullName
                 forKey:kTrainerKey];
    
    [dict safeSetObject:workout.iosBoxCoverUrl
                 forKey:kImageURLKey];
    
    [dict safeSetObject:workout.duration.stringValue
                 forKey:kDurationKey];
    
    return dict;
}

+ (NSSet *)dataTypesToWrite {
    
    HKQuantityType *heartrateType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    
    HKQuantityType *calsType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
    
    HKWorkoutType *workoutType = [HKWorkoutType workoutType];
    
    return [NSSet setWithArray:@[heartrateType, calsType, workoutType]];
}

+ (NSSet *)dataTypesToRead {
    
    return [NSSet set];
}

@end

/*
 No longer being used. Cals burned will now be tied to workout completion events
 */
//+(void)saveCaloriesBurned:(NSNumber *)calsBurned{
//    
//    if ([self canWriteToHealthKit]){
//        
//        HKQuantityType *calsType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
//        
//        HKUnit *calsUnit = [HKUnit unitFromEnergyFormatterUnit:NSEnergyFormatterUnitKilocalorie];
//        
//        HKQuantity *calsQuantity = [HKQuantity quantityWithUnit:calsUnit
//                                                    doubleValue:calsBurned.doubleValue];
//        
//        NSDate *now = [NSDate date];
//        
//        HKQuantitySample *sample = [HKQuantitySample quantitySampleWithType:calsType
//                                                                   quantity:calsQuantity
//                                                                  startDate:now
//                                                                    endDate:now];
//        [healthStore saveObject:sample
//                 withCompletion:^(BOOL success, NSError *error){
//             
//                 if (success) {
//                     NSLog(@"saved calories data");
//                 } else {
//                     NSLog(@"error saving calories data");
//                 }
//                 
//                 if (error) {
//                     NSLog(@"%@", error.localizedDescription);
//                 }
//                 
//             }];
//    }
//}
